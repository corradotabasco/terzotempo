package com.fincons.terzotempo.http;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class HttpServerDemo {

    public static void main(String[] args) throws IOException {

        HttpServer server = HttpServer.create(new InetSocketAddress(HttpUtils.DEFAULT_PORT), 0);
        server.createContext("/greet", new PoliteHttpHandler());
        server.start();

        System.out.println("Server running at: " + InetAddress.getLocalHost() + ":" + HttpUtils.DEFAULT_PORT);

    }

}
