package com.fincons.terzotempo.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class PoliteHttpHandler implements HttpHandler {

    public void handle(HttpExchange exchange) throws IOException {

        Map<String, String> params = HttpUtils.queryToMap(exchange.getRequestURI().getQuery());
        String user = params.getOrDefault(HttpUtils.PARAM_USER, HttpUtils.DEFAULT_USER);

        String response = "Hello, " + user + "!";
        System.out.println(response);

        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

}
