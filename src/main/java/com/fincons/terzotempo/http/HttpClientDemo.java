package com.fincons.terzotempo.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClientDemo {

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        String url = "http://127.0.0.1:" + HttpUtils.DEFAULT_PORT + "/greet";
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .GET()
                .build();


        HttpResponse<String> syncResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println("Got this sync response from the server: " + syncResponse.body());

        client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).
                whenComplete(((response, exception) -> {
                    System.out.println("Got this async response from the server: " + response.body());
                }));

        Thread.sleep(10000);
    }

}
