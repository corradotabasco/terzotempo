package com.fincons.terzotempo.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HttpClientProDemo {

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        String template = "http://127.0.0.1:" + HttpUtils.DEFAULT_PORT + "/greet?user=";
        List<URI> uris = Arrays.asList(
                new URI(template + "Marco"),
                new URI(template + "Cris"),
                new URI(template + "Corrado")
        );

        HttpClient client = HttpClient.newHttpClient();

        uris.stream().
                map(uri ->
                        client
                                .sendAsync(HttpRequest.newBuilder(uri).GET().build(), HttpResponse.BodyHandlers.ofString())
                                .thenApply(HttpResponse::body)).
                forEach(e -> {
                    try {
                        System.out.println(e.get());
                    } catch (InterruptedException | ExecutionException e1) {
                        e1.printStackTrace();
                    }
                });


    }

}
