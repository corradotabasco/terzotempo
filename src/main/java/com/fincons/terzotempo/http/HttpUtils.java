package com.fincons.terzotempo.http;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HttpUtils {

    public static final String PARAM_USER = "user";
    public static final String DEFAULT_USER = "World";
    public static final int DEFAULT_PORT = 1234;

    /**
     * returns the url parameters in a map
     *
     * @param query
     * @return map
     */
    public static Map<String, String> queryToMap(String query) {
        Map<String, String> result = new HashMap<>();

        if (Objects.isNull(query)) {
            return result;
        }

        for (String param : query.split("&")) {
            String[] pair = param.split("=");
            if (pair.length > 1) {
                result.put(pair[0], pair[1]);
            } else {
                result.put(pair[0], "");
            }
        }
        return result;
    }

}
