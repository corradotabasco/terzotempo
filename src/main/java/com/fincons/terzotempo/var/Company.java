package com.fincons.terzotempo.var;

public class Company {

    private String businessName;
    private Short numberOfEmployees;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Short getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(Short numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }
}
